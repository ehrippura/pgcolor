//    Copyright (C) 2011 Tzu-Yi Lin
//
//    This program is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 2
//    of the License, or (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
//    EWPGColorController.m
//    PGColor
//
//    Created by 林 子頤 on 11/12/26.
//    Copyright (c) 2011年 Eternal Wind. All rights reserved.
//

#import "EWPGColorController.h"

#define REG_SHARP_PATTERN   @"\\#([0-9]|[A-F]){6}"
#define REG_HEX_PATTERN     @"0x([0-9]|[A-F]){2}"

@implementation EWPGColorController

- (void)awakeFromNib
{
    [_colorPanel deactivate];
    [self changeColor:_colorPanel];
    [[NSColorPanel sharedColorPanel] setShowsAlpha:YES];
}

- (IBAction)copyToClip:(id)sender
{
    NSPasteboard *board = [NSPasteboard generalPasteboard];
    [board clearContents];
    [board setString:[_outputText stringValue] forType:NSPasteboardTypeString];
}

- (IBAction)changeColor:(id)sender
{
    NSColor *color = [sender color];
    CGFloat red, green, blue, alpha;

    [color getRed:&red green:&green blue:&blue alpha:&alpha];

    NSButton *button = (NSButton *)[_typeRadio selectedCell];
    NSString *format = nil;

    switch (button.tag) {
        case EWPGColorDisplayUIColor:
            format = @"[UIColor colorWithRed:%.3ff green:%.3ff blue:%.3ff alpha:%.3ff]";
            break;
        case EWPGColorDisplayNSColor:
            format = @"[NSColor colorWithCalibratedRed:%.3ff green:%.3ff blue:%.3ff alpha:%.3ff]";
            break;
        case EWPGColorDisplaySequence:
            format = @"%.3ff, %.3ff, %.3ff, %.3ff";
            break;
        case EWPGColorDisplaySharp:
            format = @"#%02X%02X%02X";
    }

    if (button.tag == 3)
        [_outputText setStringValue:[NSString stringWithFormat:format, (int)(red * 255), (int)(green * 255), (int)(blue * 255)]];
    else
        [_outputText setStringValue:[NSString stringWithFormat:format, red, green, blue, alpha]];
}

- (IBAction)matrixChange:(id)sender
{
    [self changeColor:_colorPanel];
}

- (BOOL)control:(NSControl *)control textShouldEndEditing:(NSText *)fieldEditor
{
    if (control == _outputText) {
        NSString *value = [fieldEditor string];
        NSError *err = nil; // for debug
        NSRegularExpression *regex;
        __block unsigned int red, green, blue;
        BOOL flag = NO;
        red = green = blue = 0;

        if ([value length] == 7) {
            regex = [NSRegularExpression regularExpressionWithPattern:REG_SHARP_PATTERN
                                                              options:NSRegularExpressionCaseInsensitive
                                                                error:&err];

            NSTextCheckingResult *result = [regex firstMatchInString:value options:NSMatchingReportProgress range:NSMakeRange(0, [value length])];
            if (result.range.location != NSNotFound) {
                NSScanner *scanner = [NSScanner scannerWithString:value];
                [scanner setScanLocation:1];    // ignore '#'

                unsigned int mixValue;
                [scanner scanHexInt:&mixValue];
                blue = mixValue & 0xff;
                green = (mixValue >>= 8) & 0xff;
                red = (mixValue >>= 8) & 0xff;

                flag = YES;
            }
        }
        else {
            regex = [NSRegularExpression regularExpressionWithPattern:REG_HEX_PATTERN
                                                              options:NSRegularExpressionCaseInsensitive
                                                                error:&err];

            if ([regex numberOfMatchesInString:value options:NSMatchingReportProgress range:NSMakeRange(0, [value length])] == 3) {
                __block int selector = 0;

                void (^regBlock)(NSTextCheckingResult *, NSMatchingFlags, BOOL *) = ^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop){
                    NSString *subString = [value substringWithRange:result.range];
                    NSScanner *scanner = [NSScanner scannerWithString:subString];
                    [scanner setScanLocation:2]; // skip 0x

                    switch (selector) {
                        case 0:
                            [scanner scanHexInt:&red];
                            selector++;
                            break;
                        case 1:
                            [scanner scanHexInt:&green];
                            selector++;
                            break;
                        case 2:
                            [scanner scanHexInt:&blue];
                            break;
                    }
                };

                [regex enumerateMatchesInString:value
                                        options:0
                                          range:NSMakeRange(0, [value length])
                                     usingBlock:regBlock];

                flag = YES;

            } // end of if
        } // end of else

        if (flag) {
            // set color
            [_typeRadio selectCellWithTag:EWPGColorDisplaySharp];
            NSColor *color = [NSColor colorWithSRGBRed:(CGFloat)red / 255.0 green:(CGFloat)green / 255.0 blue:(CGFloat)blue /255.0 alpha:1.0f];
            [_colorPanel setColor:color];
        }
    }

    return YES;
}

@end
