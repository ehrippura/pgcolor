//    Copyright (C) 2011 Tzu-Yi Lin
//
//    This program is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 2
//    of the License, or (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
//    EWPGColorController.h
//    PGColor
//
//    Created by 林 子頤 on 11/12/26.
//    Copyright (c) 2011年 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

// matrix tag
enum {
    EWPGColorDisplayUIColor     = 0,
    EWPGColorDisplayNSColor     = 1,
    EWPGColorDisplaySequence    = 2,
    EWPGColorDisplaySharp       = 3,
};
typedef NSInteger EWPGColorDisplayType;

@interface EWPGColorController : NSObject <NSTextFieldDelegate> {
	IBOutlet NSColorWell	*_colorPanel;
	IBOutlet NSMatrix		*_typeRadio;
	IBOutlet NSTextField	*_outputText;
}

- (IBAction)copyToClip:(id)sender;
- (IBAction)changeColor:(id)sender;
- (IBAction)matrixChange:(id)sender;

@end
